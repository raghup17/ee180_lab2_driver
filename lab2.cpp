#include "libxdma.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
using namespace cv;
using namespace std;

#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include "defs.h"
#include "utils.h"
#include "accelerator.h"
#include "globals.h"

struct opts opts;

void grayScale(Mat& img, Mat& img_gray_out)
{
  double color;

  // Convert to grayscale
  for (int i=0; i<img.rows; i++) {
    for (int j=0; j<img.cols; j++) {
      color = .114*img.data[STEP0*i + STEP1*j] + 
        .587*img.data[STEP0*i + STEP1*j + 1]+
        .299*img.data[STEP0*i + STEP1*j + 2];
      img_gray_out.data[IMG_WIDTH_BORDER * (i+BORDER) + (j+BORDER)] = color;
    }
  }
} 

int main(int argc, char** argv)
{
  Mat img, img_gray, img_sobel;
  Mat img_gray_strip, img_sobel_strip;
  CvCapture *sobel_input = NULL;
  unsigned char *instrBuffer, *txBuffer, *rxBuffer;
  char quit = -1;
  int retval = 0, bytesRead = 0;

  // Option parsing
  parseOpts(argc, argv, &opts);

  // Initialization
  retval = init();
  ASSERT(retval == 0, "Init failed\n");

  resetMIPS(); 

  resetDMA();

  // DMA buffer allocation
  instrBuffer = (unsigned char*) xdma_alloc(NUM_INSTR_WORDS, sizeof(uint32_t));
  rxBuffer = (unsigned char*) xdma_alloc(NUM_DATA_WORDS, sizeof(uint32_t));
  txBuffer = (unsigned char*) xdma_alloc(NUM_DATA_WORDS, sizeof(uint32_t));
  ASSERT(instrBuffer, "instrBuffer allocation failed\n");
  ASSERT(rxBuffer, "rxBuffer allocation failed\n");
  ASSERT(txBuffer, "txBuffer allocation failed\n");
  memset(instrBuffer, 0, NUM_INSTR_WORDS* sizeof(uint32_t));
  memset(rxBuffer, 0, NUM_DATA_WORDS * sizeof(uint32_t));
  memset(txBuffer, 0, NUM_DATA_WORDS * sizeof(uint32_t));

  // Allocate and initialize Sobel-specific data structures
  if (opts.sobelMode) {
    img_sobel = Mat(IMG_HEIGHT, IMG_WIDTH, CV_8UC1);
    img_gray = Mat(IMG_HEIGHT_BORDER, IMG_WIDTH_BORDER, CV_8UC1);
    ASSERT(img_sobel.data, "img_sobel allocation failed\n");
    ASSERT(img_gray.data, "img_gray allocation failed\n");
    memset(img_gray.data, 0, IMG_WIDTH_BORDER * IMG_HEIGHT_BORDER * sizeof(unsigned char));
    memset(img_sobel.data, 0, IMG_WIDTH * IMG_HEIGHT * sizeof(unsigned char));
  }

  // Read instruction memory file
   bytesRead = fileToBufHex(instrBuffer, opts.instBuf, NUM_INSTR_WORDS*sizeof(uint32_t*));
   EPRINTF("Program size: 0x%x bytes (%d instructions)\n", bytesRead, bytesRead/sizeof(uint32_t*));

  // Load instruction memory
  retval = dma_send_instructions((uint32_t*)instrBuffer, NUM_INSTR_WORDS);
  ASSERT(retval == 0, "dma_send_instructions failed\n");


  // Non-Sobel path: Run MIPS once, quite once it is done
  if (!opts.sobelMode) {
    // Load data memory  
      bytesRead = fileToBufHex(txBuffer, opts.inBuf, NUM_DATA_WORDS*sizeof(uint32_t*)); 
      int words = (bytesRead & 0x11) ? (bytesRead/sizeof(uint32_t*)) + 1 : bytesRead/sizeof(uint32_t*);
      EPRINTF("Input buffer size: %d bytes (%d words)\n", bytesRead, words);
      retval = dma_send_data((uint32_t*)txBuffer, NUM_DATA_WORDS);
      ASSERT(retval == 0, "dma_send_data failed\n");

      // Writing rows and cols is required only for the Sobel unit test
      uint32_t cmdReg = 0x0 | (ROWS_PER_ITER) << __builtin_ctz(CMD_ROW) | (IMG_WIDTH) << __builtin_ctz(CMD_COL);

      // Start MIPS
      u32 status = goMIPS(cmdReg);
    
      // Copy output buffer
      retval = dma_receive_data((uint32_t*)rxBuffer, NUM_DATA_WORDS);
      ASSERT(retval == 0, "dma_receive_data failed\n");

      // If output file is set, dump
      if (opts.outBuf) {
        bufToFileHex(opts.outBuf, rxBuffer, NUM_DATA_WORDS*sizeof(uint32_t*));
      }

     // Deassert the GO signal
      if (status != 0) {
        stopMIPS();
      }
  }
  else { // Sobel mode
    if (opts.webcam) {
      sobel_input = cvCreateCameraCapture(-1);
      ASSERT(sobel_input, "Error connecting to webcam. Check if your webcam is plugged in\n");
    } else if (opts.videoFile) {
      sobel_input = cvCreateFileCapture(opts.videoFile); 
      ASSERT(sobel_input, "Error opening video file '%s': Make sure the path is correct\n", opts.videoFile);
    }
    namedWindow("Sobel Display", WINDOW_AUTOSIZE);

    int frameCount = 0;
    double elapsed = 0.0;
    while (quit != 'q') {
      img = cvQueryFrame(sobel_input);
      if (!img.data) {
        EPRINTF("No more frames, quitting");
        break;
      }
      grayScale(img, img_gray);

      for (int i=0; i<IMG_HEIGHT_BORDER; i += ROWS_PER_ITER-2) {
        int inRowStart = i;
        int inRowEnd = MIN (inRowStart+ROWS_PER_ITER-1, IMG_HEIGHT_BORDER-1);
        int outRowStart =  i;
        int outRowEnd = MIN (outRowStart+ROWS_PER_ITER-2-1, IMG_HEIGHT-1);
        int colStart = 0;
        int colEnd = IMG_WIDTH_BORDER;
 
        // Set row and column values
        uint32_t rowVal = (outRowEnd-outRowStart)+1;
        uint32_t colVal = IMG_WIDTH;
        uint32_t regVal = 0x0 | rowVal << __builtin_ctz(CMD_ROW) | colVal << __builtin_ctz(CMD_COL);

        // Get the correct input and output Mat slice 
        img_gray_strip = img_gray(cv::Range(inRowStart,inRowEnd+1), cv::Range(colStart,colEnd));
        img_sobel_strip = img_sobel(cv::Range(outRowStart,outRowEnd+1), cv::Range(0,IMG_WIDTH));
        
        // Copy the input grayscale slice into the transmit buffer
        memcpy(txBuffer, img_gray_strip.data, img_gray_strip.rows*img_gray_strip.cols);
        htonlBuf((uint32_t*)txBuffer, img_gray_strip.rows*img_gray_strip.cols/4);
        retval = dma_send_data((uint32_t*)txBuffer, img_gray_strip.rows*img_gray_strip.cols/4);
        ASSERT(retval == 0, "dma_send_data failed\n");

        // Start the MIPS processor, wait for it to be done
        double tbefore = getTime();
        goMIPS(regVal);
        double tafter = getTime();
        elapsed += tafter - tbefore;

        // DMA the output buffer into its place in the output Mat 
        retval = dma_receive_data((uint32_t*)rxBuffer, img_sobel_strip.rows*img_sobel_strip.cols/4);
        ntohlBuf((uint32_t*)rxBuffer, img_sobel_strip.rows*img_sobel_strip.cols/4);
        ASSERT(retval == 0, "dma_receive_data failed\n");
        memcpy(img_sobel_strip.data, rxBuffer, img_sobel_strip.rows*img_sobel_strip.cols);

        // Deassert the Go signal, wait for MIPS to deassert done
        stopMIPS();
      }
      imshow("Sobel Display", img_sobel);
      quit = waitKey(CV_WAIT_MS);
      frameCount++;
      EPRINTF("[frame %d]\n", frameCount);
    }

    elapsed = elapsed / 1000.0;
    EPRINTF("Framecount              : %d\n", frameCount);
    EPRINTF("Total MIPS time elapsed : %lf s\n", elapsed);
    EPRINTF("Frames per second       : %f\n", (float)frameCount/elapsed);
  }
  xdma_exit();
  return 0;
}
