#ifndef __DEFS_H__
#define __DEFS_H__

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

// Image-related constants
#define IMG_WIDTH 640
#define IMG_HEIGHT 480
#define STEP1 3
#define STEP0 (IMG_WIDTH * STEP1)
#define BORDER 1
#define IMG_WIDTH_BORDER (IMG_WIDTH+2*BORDER)
#define IMG_HEIGHT_BORDER (IMG_HEIGHT+2*BORDER)
#define MAX_FRAMES 300

#define ROWS_PER_ITER 164 
#define COLS_PER_ITER IMG_WIDTH_BORDER
#define CV_WAIT_MS 10
#define MIPS_WAIT_TIME 5   // MIPS wait time in seconds

// Memory mapping related constants
#define MAP_LEN 0x10000
#define XPAR_STREAM_ENGINE_0_BASEADDR 0xBFC00000
#define XPAR_AXIDMA_0_BASEADDR        0x80400000
#define XPAR_AXIDMA_MM2S_CR_OFFSET         0x0
#define XPAR_AXIDMA_MM2S_SR_OFFSET         0x4
#define XPAR_AXIDMA_S2MM_CR_OFFSET         0x30
#define XPAR_AXIDMA_S2MM_SR_OFFSET         0x34
#define XPAR_AXIDMA_SR_IDLE_MASK      0x2
static const size_t NUM_INSTR_WORDS = 16384;
static const size_t NUM_DATA_WORDS = 32768;

typedef unsigned long u32;

static const u32 TEST_PATTERN = 0xc001cafe;

// Bit masks and positions - Command register
#define YES 0x1
#define CMD_ROW   0x00000FFC
#define CMD_COL   0x003FF000
#define CMD_GO    0x00000001
#define CMD_RESET 0x00000010

// Bit masks and positions - Status register
#define ST_DONE  0x00000001
#define ST_ERR   0x00000010
#define ST_STATE 0x0000003C

#define MREAD(val, mask) (((val) & (mask)) >> __builtin_ctz(mask))
#define MWRITE(val, mask) (((val) << __builtin_ctz(mask)) & (mask))

// Some helper macros
#define EPRINTF(...) fprintf(stderr, __VA_ARGS__)
#define ASSERT(cond, ...) \
  if (!(cond)) { \
    EPRINTF("\n");        \
    EPRINTF(__VA_ARGS__); \
    EPRINTF("\n");        \
    EPRINTF("Assertion (%s) failed in %s, %d\n", #cond, __FILE__, __LINE__); \
    assert(0);  \
  }

#endif  // ifndef __DEFS_H__
