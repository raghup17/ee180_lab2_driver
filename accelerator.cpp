#include <string.h>
#include <fcntl.h>
#include <sys/mman.h>
#include "libxdma.h"
#include "defs.h"
#include "utils.h"
#include "accelerator.h"
#include "globals.h"

// Stream engine virtual addresses
static int fd = 0;
static u32 se_command = 0;
static u32 se_input_buf_select = 0; 
static u32 se_packet_size = 0;
static u32 se_start_address_read = 0;
static u32 se_start_address_write = 0;
static u32 se_test = 0; 
static u32 se_status = 0; 

static u32 dma_base = 0;
static u32 dma_mm2s_cr = 0;
static u32 dma_mm2s_sr = 0;
static u32 dma_s2mm_cr = 0;
static u32 dma_s2mm_sr = 0;


void printRegs(const char *msg)
{
  if (opts.debug) {
    EPRINTF("%s\n", msg);
    EPRINTF("------- MIPS cmd and status registers: ---------\n");
    EPRINTF("CMD     : %.8lx\n", Xil_In32(se_command));
    EPRINTF("STATUS  : %.8lx\n", Xil_In32(se_status));
    EPRINTF("TEST    : %.8lx\n", Xil_In32(se_test));
    EPRINTF("------------------------------------------------\n");
  }
}

void printDMARegs()
{
  if (opts.debug) {
    EPRINTF("------- DMA cmd and status registers: ---------\n");
    EPRINTF("MM2S CR = %.8lx\n", Xil_In32(dma_mm2s_cr));
    EPRINTF("S2MM CR = %.8lx\n", Xil_In32(dma_s2mm_cr));
    EPRINTF("MM2S SR = %.8lx\n", Xil_In32(dma_mm2s_sr));
    EPRINTF("S2MM SR = %.8lx\n", Xil_In32(dma_s2mm_sr));
    EPRINTF("------------------------------------------------\n");
  }
}

void resetDMA()
{
  Xil_Out32(dma_s2mm_cr, 0x4);
  u32 dma_cr = Xil_In32(dma_s2mm_cr);
  while (dma_cr & 0x4) {
    dma_cr = Xil_In32(dma_s2mm_cr);
  }

  Xil_Out32(dma_mm2s_cr, 0x4);
  dma_cr = Xil_In32(dma_mm2s_cr);
  while (dma_cr & 0x4) {
    dma_cr = Xil_In32(dma_mm2s_cr);
  }

  Xil_Out32(dma_s2mm_cr, XILINX_DMA_XR_IRQ_ALL_MASK);
  Xil_Out32(dma_mm2s_cr, XILINX_DMA_XR_IRQ_ALL_MASK);

  printDMARegs();  
}

int dma_init()
{
  void *ptr;
  ptr = mmap(NULL, MAP_LEN, PROT_READ|PROT_WRITE, MAP_SHARED, fd, XPAR_AXIDMA_0_BASEADDR);
  dma_base = (u32) ptr;
  dma_mm2s_sr   = dma_base + XPAR_AXIDMA_MM2S_SR_OFFSET;
  dma_mm2s_cr   = dma_base + XPAR_AXIDMA_MM2S_CR_OFFSET;
  dma_s2mm_sr   = dma_base + XPAR_AXIDMA_S2MM_SR_OFFSET;
  dma_s2mm_cr   = dma_base + XPAR_AXIDMA_S2MM_CR_OFFSET;
  return 0;
}

int se_init()
{
    void *ptr;
    ptr = mmap(NULL, MAP_LEN, PROT_READ|PROT_WRITE, MAP_SHARED, fd, XPAR_STREAM_ENGINE_0_BASEADDR);
    se_command = (u32) ptr; 
    se_input_buf_select = se_command + sizeof(u32);
    se_packet_size = se_command + 2*sizeof(u32);
    se_start_address_read = se_command + 3*sizeof(u32);
    se_start_address_write = se_command + 4*sizeof(u32);
    se_test = se_command + 5*sizeof(u32);
    se_status = se_command + 6*sizeof(u32);
    return 0;
}

int init()
{
	// open /dev/mem file
  int retval = setuid(0);
	ASSERT(retval == 0, "setuid(0) failed\n");
	fd = open ("/dev/mem", O_RDWR);
	if (fd < 1) {
		perror("error opening /dev/mem\n");
		return -1;
	}

	// Initialize xdma library
	if (xdma_init() < 0) {
		return -1;
	}

	// Initialize pointers to stream_engine IPIF registers
	se_init();

  dma_init();
	return 0; 
}

int dma_send_instructions(uint32_t *instBuffer, u32 numInstr)
{
  resetDMA();
  Xil_Out32(se_input_buf_select, 1); // select instruction buffer
  Xil_Out32(se_start_address_write, 0); // set input data start address to 0

#if 0
  printf("The selected input buffer is %lu\n", Xil_In32(se_input_buf_select));
  printf("The start address for writing is %lu\n", Xil_In32(se_start_address_write));
#endif

  int retval = xdma_perform_transaction(0, XDMA_WAIT_SRC, instBuffer, numInstr, NULL, (uint32_t)NULL);
  ASSERT(retval == 0, "Instruction DMA failed\n");
  u32 dma_status = Xil_In32(dma_mm2s_sr);
  while (!(dma_status & XPAR_AXIDMA_SR_IDLE_MASK)) {
    dma_status = Xil_In32(dma_mm2s_sr);
  }

  return 0;
}

int dma_send_data(uint32_t *txBuffer, u32 numWords)
{
  resetDMA();
	Xil_Out32(se_input_buf_select, 2); // select input data buffer
  Xil_Out32(se_start_address_write, 0); // set input data start address to 0

#if 0
  printf("The selected input buffer is %lu\n", Xil_In32(se_input_buf_select));
  printf("The start address for writing is %lu\n", Xil_In32(se_start_address_write));
#endif

	int retval = xdma_perform_transaction(0, XDMA_WAIT_SRC, txBuffer, numWords, NULL, (uint32_t)NULL);
  ASSERT(retval == 0, "DMA send data failed\n");
  u32 dma_status = Xil_In32(dma_mm2s_sr);
  while (!(dma_status & XPAR_AXIDMA_SR_IDLE_MASK)) {
    dma_status = Xil_In32(dma_mm2s_sr);
  }
	return 0;
}

int dma_receive_data(uint32_t *rxBuffer, u32 numWords)
{
  resetDMA();
	Xil_Out32(se_start_address_read, 0); // set input data start address to 0
  Xil_Out32(se_packet_size, numWords); // set transfer size to the data size (in words)

#if 0
  printf("The start address for reading is %lu\n", Xil_In32(se_start_address_read));
  printf("The packet size is %lu\n", Xil_In32(se_packet_size));
#endif

	int retval = xdma_perform_transaction(0, XDMA_WAIT_DST, NULL, (uint32_t)NULL, rxBuffer, numWords);
  ASSERT(retval == 0, "DMA receive data failed\n");
  u32 dma_status = Xil_In32(dma_s2mm_sr);
  while (!(dma_status & XPAR_AXIDMA_SR_IDLE_MASK)) {
    dma_status = Xil_In32(dma_s2mm_sr);
  }

	return 0;
}

int goMIPS(uint32_t cmdReg)
{
  u32 status = Xil_In32(se_status);

  printRegs("Before Starting MIPS");

  // Start the MIPS processor
  u32 oldcmd = Xil_In32(se_command);
  ASSERT(!(oldcmd & 0x1), "MIPS is already running!\n");
  u32 newcmd = cmdReg | 0x1;
  Xil_Out32(se_command, newcmd);

  printRegs("Starting MIPS");

  // Wait for completion signal
  status = Xil_In32(se_status);
  u32 counter = 0;
  double tbefore = getTime();
  while (!(status & 0x1)) {
    status = Xil_In32(se_status);
    if (opts.debug) {
      double tafter = getTime();
      double elapsed = tafter - tbefore;
      if (elapsed >= 1000.0) {
        counter++;
        EPRINTF("(%ld) ", counter);
        printRegs("Waiting for MIPS..");
        tbefore = tafter;
      }
    }
    if (counter >= MIPS_WAIT_TIME) {
      EPRINTF("Waited long enough, not waiting anymore\n");
      return status;
    }
  }
 
  printRegs("MIPS done");
  return status;
}

int stopMIPS()
{
  u32 status = Xil_In32(se_status);
  // Stop the MIPS processor
  Xil_Out32(se_command, 0x0);

  printRegs("Stopped MIPS");
  // Wait for MIPS to drop done
  status = Xil_In32(se_status);
  while ((status & 0x1)) {
    status = Xil_In32(se_status);
  }

  printRegs("Status deasserted");
  status = Xil_In32(se_status);
  return status;
}

void resetMIPS()
{
  Xil_Out32(se_command, 0x2);
}

void unresetMIPS()
{
  Xil_Out32(se_command, 0x0);
}

