#ifndef __ACCELERATOR_H__
#define __ACCELERATOR_H__

#define XILINX_DMA_XR_IRQ_ALL_MASK  0x00007000 /* All interrupts */

int dma_send_instructions(uint32_t *instBuffer, u32 numInstr);
int dma_send_data(uint32_t *txBuffer, u32 numWords);
int dma_receive_data(uint32_t *rxBuffer, u32 numWords);
void printRegs(const char *msg);
void printDMARegs();
void resetDMA();
int dma_init();
int se_init();
int init();

int goMIPS(uint32_t cmdReg);
int stopMIPS();
void resetMIPS();
void unresetMIPS();


#endif  // __ifndef __ACCELERATOR_H__
