# Set compiler args
CC=g++
CFLAGS=-Wall -c -O3 -ftree-vectorize
LDFLAGS=
LDLIBS=-L /usr/lib $$(pkg-config --cflags --libs opencv) -pthread # -lxdma
ifeq ($(shell arch), armv7l)
	LDLIBS += -lpfm 
endif
SOURCES=libxdma.cpp utils.cpp accelerator.cpp lab2.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=lab2
TAR=${EXECUTABLE}.tar.gz
SUBMIT_FILES=${EXECUTABLE}_tar/*.cpp \
	     ${EXECUTABLE}_tar/*.h  \
	     ${EXECUTABLE}_tar/Makefile

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE):$(OBJECTS)
	$(CC) -o $@ $(LDFLAGS) $(OBJECTS) $(LDLIBS)
	utils/set_perms $(EXECUTABLE)

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	\rm -f *.o $(EXECUTABLE) $(TAR)

submit: clean
	rm -f ${EXECUTABLE}_tar
	ln -s . ${EXECUTABLE}_tar
	tar -czf $(TAR) $(SUBMIT_FILES)
	rm -f ${EXECUTABLE}_tar
